REM  *****  BASIC  *****
Dim oDoc As Object
Dim oForm As Object
Dim oSheet As Object
Dim oSheet2 As Object
Dim oCell As Object
Dim oCell2 As Object
Dim oCheckBox As Object
Dim oRadioButton_32bit As Object
Dim oRadioButton_64bit As Object
Dim col As Integer
Dim row As Integer
Dim eip As Integer

'Funkcje i procedury formatujące arkusz

Sub _colour_cell(oSheet As Object, color As String, col As Integer, row As Integer)

	oCell = oSheet.getCellByposition(col,row)

	If color = "gray" Then
		oCell.CellBackColor = RGB(233, 233 ,233)
	ElseIf color = "white" Then
		oCell.CellBackColor = RGB(255, 255 ,255)
	End If

End Sub

Sub _font_style(oSheet As Object, style As String, col As Integer, row As Integer)

	oCell = oSheet.getCellByposition(col,row)

	If style = "bold" Then
		oCell.CharWeight = com.sun.star.awt.FontWeight.BOLD
	ElseIf style = "normal" Then
		oCell.CharWeight = com.sun.star.awt.FontWeight.NORMAL
	End If

End Sub

'Funkcje i procedury wykonujące kod asemblera
'TODO: Do dokonczenia
Sub _move()
	print "jestem metodoa move do dokonczenia"
End Sub

Function _take_code(oSheet As Object, col As Integer, row As Integer) As String

	oCell = oSheet.getCellByposition(col,row)
	_take_code = oCell.getString

End Function

Sub _run_code(code As String)

	Select Case code
		Case Is = "mov"
			_move()
			If oCheckBox.State = 1 Then
				_info(code)
			End If
	End Select

End Sub

'Funkcje i procedury obsługujące przyciski

Sub _info(code As String)

	oSheet2=oDoc.Sheets.getByName("Podpowiedź")

	Dim i As Integer

	For i = 1 To 1000 Step 1
	   oCell2=oSheet2.getCellByposition(0,i)
	   If oCell.getString() = code Then
		   oCell2=oSheet2.getCellByposition(1,i)
		   MsgBox(oCell2.getString())
		   Exit For
	   End If
	Next i

End Sub

Function search_ip(col As Integer) As Integer

	 Dim i As Integer

     For i = 1 To 1000 Step 1
     	oCell=oSheet.getCellByposition(col,i)
     	If oCell.getString() = "EIP" Or oCell.getString() = "RIP" Then
     		search_ip = i
     		Exit For
     	End If
     Next i

End Function

Sub move_eip()

	Main()
	Dim code As String
	'Pobieranie kodu
	code = _take_code(oSheet, col+1, row)
	_run_code(code)

	'Formatowanie arkusza
	oCell=oSheet.getCellByposition(col,row)
	oCell.SetString("")
	row = row + 1
	oCell=oSheet.getCellByposition(col,row)

	If oRadioButton_64bit.State = 1 Then
		oCell.SetString("RIP")
	Else
		oCell.SetString("EIP")
	End If


	_colour_cell(oSheet, "white", col, row-1)
	_colour_cell(oSheet, "white", col-1, row-1)
	_font_style(oSheet, "normal", col-1, row-1)
	_colour_cell(oSheet, "gray", col, row)
	_colour_cell(oSheet, "gray", col-1, row)
	_font_style(oSheet, "bold", col-1, row)

End Sub

Sub beck()

	Main()
	If row > 1 Then
		oCell=oSheet.getCellByposition(col,row)
		oCell.SetString("")

		_colour_cell(oSheet, "white", col, row)
		_colour_cell(oSheet, "white", col-1, row)
		_font_style(oSheet, "normal", col-1, row)

		row = row - 1
		oCell=oSheet.getCellByposition(col,row)

	If oRadioButton_64bit.State = 1 Then
		oCell.SetString("RIP")
	Else
		oCell.SetString("EIP")
	End If

		_colour_cell(oSheet, "gray", col, row)
		_colour_cell(oSheet, "gray", col-1, row)
		_font_style(oSheet, "bold", col-1, row)

	End If

End Sub

Sub bit_32()

	Main()

	If oRadioButton_64bit.State = 1 Then
		oRadioButton_64bit.State = 0
	End If

	oCell=oSheet.getCellByposition(col,row)
	oCell.SetString("EIP")

End Sub

Sub bit_64()

	Main()

	If oRadioButton_32bit.State = 1 Then
		oRadioButton_32bit.State = 0
	End If

	oCell=oSheet.getCellByposition(col,row)
	oCell.SetString("RIP")

End Sub

Sub Main

	oDoc = ThisComponent
	oSheet = oDoc.Sheets.getByName("kod")
	col = 3
	row = search_ip(col)

	'oForm=ThisComponent.Sheets(0).DrawPage.Forms.getByIndex(0)
	oForm = oSheet.DrawPage.Forms.getByIndex(0)
	oCheckBox = oForm.getByName("CheckBox1")

	oRadioButton_64bit = oForm.getByName("64bity")
	oRadioButton_32bit = oForm.getByName("32bity")

End Sub
